package org.intellij.sdk.listener;

import com.intellij.openapi.project.Project;
import com.intellij.util.messages.Topic;
import org.jetbrains.annotations.NotNull;

/**
 * @author TWH
 */
public class DateRefreshMessagePublisher {
    public static final Topic<DateRefreshListener> TOPIC = new Topic<>("date refresh events", DateRefreshListener.class);

    public static DateRefreshMessagePublisher getInstance(Project project) {
        return (DateRefreshMessagePublisher) project.getPicoContainer().getComponentInstanceOfType(DateRefreshMessagePublisher.class);
    }

    /**
     * 推送刷新事件
     */
    public void fireDateRefreshExecute(String date, Project project) {
        getPublisher(project).refresh(date, project);
    }

    @NotNull
    private static DateRefreshListener getPublisher(Project project) {
        return project.getMessageBus().syncPublisher(TOPIC);
    }
}
