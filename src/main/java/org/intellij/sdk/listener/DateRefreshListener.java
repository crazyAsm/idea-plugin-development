package org.intellij.sdk.listener;

import com.intellij.openapi.project.Project;

/**
 * @author TWH
 */
public interface DateRefreshListener {
    /**
     * 时间刷新事件处理方法
     */
    void refresh(String date, Project fireProject);
}
