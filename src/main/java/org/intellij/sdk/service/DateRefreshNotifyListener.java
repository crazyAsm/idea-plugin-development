package org.intellij.sdk.service;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.project.Project;
import org.intellij.sdk.NotifyGroupConstants;
import org.intellij.sdk.listener.DateRefreshListener;

/**
 * @author TWH
 */
public class DateRefreshNotifyListener implements DateRefreshListener {

    @Override
    public void refresh(String date, Project fireProject) {
        Notification notify = NotifyGroupConstants.NOTIFICATION_GROUP
                .createNotification("通过plugin.xml定义的-Listener给" + fireProject.getName() + "发通知" + date, NotificationType.INFORMATION);
        notify.notify(fireProject);
    }
}
