// Copyright 2000-2020 JetBrains s.r.o. and other contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.

package org.intellij.sdk.toolWindow;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import org.intellij.sdk.listener.DateRefreshMessagePublisher;

import javax.swing.*;
import java.util.Calendar;

public class MyToolWindow {

    private JButton refreshToolWindowButton;
    private JButton hideToolWindowButton;
    private JLabel currentDate;
    private JLabel currentTime;
    private JLabel timeZone;
    private JPanel myToolWindowContent;

    public MyToolWindow(ToolWindow toolWindow, Project project) {
        hideToolWindowButton.addActionListener(e -> toolWindow.hide(null));
        refreshToolWindowButton.addActionListener(e -> {
            currentDateTime();
            // 触发刷新时间
            DateRefreshMessagePublisher.getInstance(project).fireDateRefreshExecute(currentTime.getText(), project);
        });

        this.currentDateTime();
    }

    public void currentDateTime() {
        // Get current date and time
        Calendar instance = Calendar.getInstance();
        currentDate.setText(
                instance.get(Calendar.DAY_OF_MONTH) + "/"
                        + (instance.get(Calendar.MONTH) + 1) + "/"
                        + instance.get(Calendar.YEAR)
        );
        currentDate.setIcon(new ImageIcon(getClass().getResource("/toolWindow/Calendar-icon.png")));
        int min = instance.get(Calendar.MINUTE);
        int sec = instance.get(Calendar.SECOND);

        String strMin = min < 10 ? "0" + min : String.valueOf(min);
        String strSec = min < 10 ? "0" + min : String.valueOf(sec);
        currentTime.setText(instance.get(Calendar.HOUR_OF_DAY) + ":" + strMin + ":" + strSec);
        currentTime.setIcon(new ImageIcon(getClass().getResource("/toolWindow/Time-icon.png")));
        // Get time zone
        long gmtOffset = instance.get(Calendar.ZONE_OFFSET);
        String strGmtOffset = String.valueOf(gmtOffset / 3600000);
        strGmtOffset = (gmtOffset > 0) ? "GMT + " + strGmtOffset : "GMT - " + strGmtOffset;
        timeZone.setText(strGmtOffset);
        timeZone.setIcon(new ImageIcon(getClass().getResource("/toolWindow/Time-zone-icon.png")));
    }

    public JPanel getContent() {
        return myToolWindowContent;
    }

}
