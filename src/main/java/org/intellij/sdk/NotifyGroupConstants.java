package org.intellij.sdk;

import com.intellij.notification.NotificationDisplayType;
import com.intellij.notification.NotificationGroup;

/**
 * @author TWH
 */
public class NotifyGroupConstants {
    public static final NotificationGroup NOTIFICATION_GROUP
            = new NotificationGroup("Notify", NotificationDisplayType.BALLOON, true);
}
