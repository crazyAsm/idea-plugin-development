# IDEA插件开发-[Manual][docs]
插件开发过程中用到的各种功能样例；

1. toolWindow；[Manual][docs:tool_windows]
2. idea插件消息传递机制；[Manual][docs:messaging-infrastructure]

[docs]: https://plugins.jetbrains.com/docs/intellij/
[docs:tool_windows]: https://plugins.jetbrains.com/docs/intellij/tool-windows.html
[docs:messaging-infrastructure]: https://plugins.jetbrains.com/docs/intellij/messaging-infrastructure.html
